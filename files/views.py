from django.shortcuts import render
from rest_framework import routers, serializers, viewsets
from .serializers import UserSerializer, FileSerializer, TagSerializer
from django.contrib.auth.models import User
from .models import File, Tag



class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class FileViewSet(viewsets.ModelViewSet):
    queryset = File.objects.all()
    serializer_class = FileSerializer


class TagViewSet(viewsets.ModelViewSet):
    queryset = Tag.objects.all()
    serializer_class = TagSerializer