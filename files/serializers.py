from django.contrib.auth.models import User
from files.models import File, Tag
from rest_framework import serializers

# class UserSerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = User
#         fields = ('url','id','username','is_staff')

# class FileSerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = File
#         fields = ('user','upload_file','file_name','file_description','creted','updated','private','hsh')


# class UserSerializer(serializers.Serializer):
#     id = serializers.IntegerField()
#     username = serializers.CharField(max_length=200)
#     password = serializers.CharField(max_length=100, style={"input_type":"password"})
#     is_staff = serializers.BooleanField(default=False)

#     def create(self, validated_data):
#         return User.objects.create(**validated_data)

#     def update(self, instance, validated_data):
#         instance.username = validated_data.get('username', instance.username)
#         instance.password = validated_data.get('password', instance.password)
#         instance.is_staff = validated_data.get('is_staff', instance.is_staff)
#         instance.save()
#         return instance


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id','username','email','is_staff')


class FileSerializer(serializers.ModelSerializer):
    class Meta:
        model = File
        fields = ('user','upload_file','file_name','file_description','creted','updated','private','hsh')


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ('title','timestamp','files')