from django.db import models
from django.conf import settings
from django.db.models.signals import post_save, pre_save
import os
import hashlib



User = settings.AUTH_USER_MODEL


def get_filename(filepath):
    base_name = os.path.basename(filepath)
    name, ext = os.path.splitext(base_name)
    return name, ext


def upload_file_path(instance, filename):
    name, ext = get_filename(filename)
    instance.file_name = name
    return 'uploaded_files/{name}{ext}'.format(name=name, ext=ext)



class File(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    upload_file = models.FileField(upload_to=upload_file_path, blank=True, null=True)
    file_name = models.CharField(max_length=250,blank=True)
    file_description = models.CharField(max_length=500)
    creted = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    private = models.BooleanField(default=False)
    hsh = models.CharField(max_length=1000, blank=True)

    def __str__(self):
        return str(self.file_name)


def pre_save_hash_receiver(sender, instance, *args, **kwargs):
    if not instance.hsh:
        instance.hsh = hashlib.sha512(instance.file_name.encode('utf-8')).hexdigest()


pre_save.connect(pre_save_hash_receiver, sender=File)



class Tag(models.Model):
    title = models.CharField(max_length=50)
    timestamp = models.DateTimeField(auto_now_add=True)
    files = models.ManyToManyField(File, blank=True)

    def __str__(self):
        return self.title