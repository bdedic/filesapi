from django.contrib import admin
from django.urls import path, include
from django.contrib.auth.models import User
from files.models import File, Tag
from rest_framework import routers
from files.views import UserViewSet, FileViewSet, TagViewSet


router = routers.DefaultRouter()
router.register(r'api/users', UserViewSet, basename='users')
router.register(r'api/files', FileViewSet, basename='files')
router.register(r'api/tags', TagViewSet, basename='tags')



urlpatterns = [
    path('admin/', admin.site.urls),
    path(r'', include(router.urls)),
    path(r'api/', include('rest_framework.urls', namespace='rest_framework'))
]
